package com.springMySQL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJpaAndMySqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJpaAndMySqlApplication.class, args);
	}

}
