package com.springMySQL.services;

import java.util.List;

import com.springMySQL.models.User;

public interface UserService {

	public List<User> findAll();

	public void save(User user);

	public User findById(Long id);

	public void delete(Long id);
	
}
