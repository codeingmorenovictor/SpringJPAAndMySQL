package com.springMySQL.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.springMySQL.models.User;
import com.springMySQL.services.UserService;

@Controller
@SessionAttributes(value = "users")
@RequestMapping("/users")
public class UserController  {
	
	private static final String SAVE_EDIT_FORM = "saveUserForm";
	

	@Value("${global.appTitle}")
	private String appTitle;

	@Value("${user.header}")
	private String header;

	@Value("${user.saveForm.header}")
	private String saveFormHeader;

	@Value("${user.editForm.header}")
	private String editFormHeader;
	
	@Autowired
	private UserService userService;

	@GetMapping(value = { "/list" })
	public String listUsers(Model model) {
		model.addAttribute("title", appTitle);
		model.addAttribute("header", header);
		model.addAttribute("usersList", userService.findAll());
		return "users";
	}

	@GetMapping(value = "/save")
	public String createUserFromForm(Model model) {
		User c = new User();
		model.addAttribute("title", appTitle);
		model.addAttribute("header", saveFormHeader);
		model.addAttribute("user", c);
		return SAVE_EDIT_FORM;
	}

	// This method will redirect to "list" GetMapping path
	// @Valid performs all the validations from the used validation annotations
	// @Valid and BindingResult MUST BE DECLARED TOGETHER
	@PostMapping(value = "/save")
	public String saveUser(@Valid User user, BindingResult validations, Model model,
			SessionStatus session) {
		if (validations.hasErrors()) {
			model.addAttribute("title", appTitle);
			model.addAttribute("header", saveFormHeader);
			return SAVE_EDIT_FORM;
		}
		userService.save(user);
		session.setComplete(); // After Persist or Merge we set the session that contains the current Bean as
								// completed, this won�t keep session data anymore
		return "redirect:list";
	}
	
	@GetMapping(value = "/save/{id}")
	public String edit(@PathVariable Long id, Model model) {
		User c = null;
		if (id > 0) {
			c = userService.findById(id);
		} else {
			return "redirect:list";
		}
		model.addAttribute("title", appTitle);
		model.addAttribute("header", editFormHeader);
		model.addAttribute("user", c);
		return SAVE_EDIT_FORM;
	}

	@GetMapping(value = "/delete/{id}")
	public String delete(@PathVariable Long id) {
		if (id > 0) {
			userService.delete(id);
		}
		return "redirect:/users/list";
	}

	

}
