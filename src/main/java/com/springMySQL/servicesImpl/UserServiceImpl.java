package com.springMySQL.servicesImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springMySQL.models.User;
import com.springMySQL.repositories.UserRepository;
import com.springMySQL.services.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userCrudRepository;

	@Override
	@Transactional(readOnly = true)
	public List<User> findAll() {
		return userCrudRepository.findAll();
	}

	@Override
	@Transactional
	public void save(User user) {
		userCrudRepository.save(user);
	}

	@Override
	@Transactional(readOnly = true)
	public User findById(Long id) {
		return userCrudRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		userCrudRepository.deleteById(id);
	}

}
