package com.springMySQL.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@Entity
@Table(schema = "springboot", name = "users")
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2549388142936551069L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty
	private String user_name;
	
	@NotEmpty
	private String password;
	
	@NotEmpty
	private String section;

	@NotNull // Works for Dates, Numbers etc data types MUST IMPLEMENT @Valid WHEN INVOQUE
	@Temporal(TemporalType.DATE)
	@Column(name = "registration_Date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date registrationDate;

	@PrePersist
	public void prePersist() {
		registrationDate = new Date();
	}

}
